﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';
import * as WeatherForecasts from '../store/WeatherForecasts';


type CounterProps =
    CounterStore.CounterState
    & typeof CounterStore.actionCreators
    & RouteComponentProps<{}>;

class Chart extends React.Component<CounterProps, {}> {

    componentDidMount() {
    }

    public render() {
     //  const script = document.createElement("script");
     //
     //  script.src = "/scripts/F.js";
     //  script.async = true;
     //
     //  document.body.appendChild(script);
     //

       return <div>
           <div><a href="/Home/Index2">пока что крывая ссылка, потом придумаю че нить!</a></div>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.counter, // Selects which state properties are merged into the component's props
    CounterStore.actionCreators                 // Selects which action creators are merged into the component's props
)(Chart) as typeof Chart;