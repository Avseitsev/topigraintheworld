﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';
import * as WeatherForecasts from '../store/WeatherForecasts';
import axios from 'axios';

type CounterProps =
    CounterStore.CounterState
    & typeof CounterStore.actionCreators
    & RouteComponentProps<{}>;

var a = "true";
//var str = "";
//var str2 = "";

interface RegState {
    status: string;
    str: string;
    str2: string;
}

export class GameOne extends React.Component<CounterProps, RegState> {
    constructor() {
        super();
        this.state = { status: "true",str:"",str2:"" };
    }

    componentWillMount() {

      try {
          if (localStorage.getItem('reg') != "true") {
              this.props.history.push('/');
          }
      }
      catch (e)
      {
          this.props.history.push('/');
      }
    }

    componentWillUnmount() {
        a = "false";
    }

    componentDidMount() {
        var z = this;
       axios.get("api/Record" + "/rec")
         .then(function (response) {
             z.setState({ str: response.data });
         })
         .catch(function (error) {
             console.log(error);
          });

         axios.get("api/Record" + "/recW")
         .then(function (response) {
             z.setState({ str2: response.data });
         })
         .catch(function (error) {
             console.log(error);
         });
    
        var name = "";
        axios.get("api/my" + "/name")
            .then(function (response) {
                name = response.data;
            })
            .catch(function (error) {
                console.log(error);
            });

        a = "true";

        var canvas: any = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        var z = this;
        var time = 0;

        function func() {
            time = time + 1;
        }
        setInterval (func, 1000);
     
       var ballRadius = 11;
       var x = canvas.width / 2;
       var y = canvas.height - 30;
       var dx = 0.9;
       var dy = -0.9;
       var paddleHeight = 10;
       var paddleWidth = 95;
       var paddleX = (canvas.width - paddleWidth) / 2;
       var rightPressed = false;
       var leftPressed = false;
       var brickRowCount = 9;
       var brickColumnCount = 6;
       var brickWidth = 45;
       var brickHeight = 18;
       var brickPadding = 10;
       var brickOffsetTop = 30;
       var brickOffsetLeft = 30;
       var score = 0;
       var lives = 2;    
       var bricks: any = [];

       for (var c = 0; c < brickColumnCount; c++) {
           bricks[c] = [];
           for (var r = 0; r < brickRowCount; r++) {
               bricks[c][r] = { x: 0, y: 0, status: 1 };
           }
       }
     
       document.addEventListener("keydown", keyDownHandler, false);
       document.addEventListener("keyup", keyUpHandler, false);
     
       function keyDownHandler(e: any) {
           if (e.keyCode == 39) {
               rightPressed = true;
           }
           else if (e.keyCode == 37) {
               leftPressed = true;
           }
       }

       function keyUpHandler(e: any) {
           if (e.keyCode == 39) {
               rightPressed = false;
           }
           else if (e.keyCode == 37) {
               leftPressed = false;
           }
       }
     
       function collisionDetection() {
           for (var c = 0; c < brickColumnCount; c++) {
               for (var r = 0; r < brickRowCount; r++) {
                   var b = bricks[c][r];
                   if (b.status == 1) {
                       if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                           dy = -dy;
                           b.status = 0;
                           score++; 
                           if (dx > 0) {
                               dx = dx + 0.05;
                           }
                           else {
                               dx = dx - 0.05;
                           }
                           if (dy > 0) {
                               dy = dy + 0.1;
                           }
                           else {
                               dy = dy - 0.1;
                           }
                           paddleWidth = paddleWidth - 1.5;
                           if (ballRadius > 3) {
                               ballRadius = ballRadius - 0.5;
                           }
                           if (score == brickRowCount * brickColumnCount) {
                             //  var xhr = new XMLHttpRequest();
                             //  xhr.open("get", "api/Record" + "/newRec?Count=" + score + "&Time=" + time, false);//false-синхронный
                             //  xhr.setRequestHeader("Accept", "application/json");
                             //  xhr.onload = function () {
                             //  };
                             //  xhr.send();
                               axios.get("api/Record" + "/newRec?Count=" + score + "&Time=" + time)
                                   .then(function (response) {
                                   })
                                   .catch(function (error) {
                                       console.log(error);
                                   });

                               alert("YOU WIN, CONGRATS!");
                               z.props.history.push('/');
                           }
                       }
                   }
               }
           }
       }
     
       function drawBall() {
           ctx.beginPath();
           ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
           ctx.fillStyle = "#0095DD";
           ctx.fill();
           ctx.closePath();
       }

       function drawPaddle() {
           ctx.beginPath();
           ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
           ctx.fillStyle = "#0095DD";
           ctx.fill();
           ctx.closePath();
       }

       function drawBricks() {
           for (var c = 0; c < brickColumnCount; c++) {
               for (var r = 0; r < brickRowCount; r++) {
                   if (bricks[c][r].status == 1) {
                       var brickX = (r * (brickWidth + brickPadding)) + brickOffsetLeft;
                       var brickY = (c * (brickHeight + brickPadding)) + brickOffsetTop;
                       bricks[c][r].x = brickX;
                       bricks[c][r].y = brickY;
                       ctx.beginPath();
                       ctx.rect(brickX, brickY, brickWidth, brickHeight);
                       ctx.fillStyle = "#0095DD";
                       ctx.fill();
                       ctx.closePath();
                   }
               }
           }
       }

       function drawScore() {
           ctx.font = "16px Arial";
           ctx.fillStyle = "#000000";
           ctx.fillText("Score: " + score, 8, 20);
       }

       function drawTime() {
           ctx.font = "16px Arial";
           ctx.fillStyle = "#000000";
           ctx.fillText("Time: " + time, 95, 20);
       }

      function drawName() {
          ctx.font = "16px Arial";
          ctx.fillStyle = "#000000";
          ctx.fillText("Name: " + name, 185, 20);
      }

      function drawLives() {
          ctx.font = "16px Arial";
          ctx.fillStyle = "#000000";
          ctx.fillText("Lives: " + lives, canvas.width - 65, 20);
      }
     
      function draw() {
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          drawBricks();
          drawBall();
          drawPaddle();
          drawScore();
          drawLives();
          drawTime();
          drawName();
          collisionDetection();
     
          if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
              dx = -dx;
          }

          if (y + dy < ballRadius) {
              dy = -dy;
          }
          else if (y + dy > canvas.height - ballRadius) {
              if (x > paddleX-6 && x < paddleX + paddleWidth+6) {
                  dy = -dy;
              }
              else {
                  lives--;
                  if (!lives) {
                     // var xhr = new XMLHttpRequest();
                     // xhr.open("get", "api/Record" + "/newRec?Count=" + score + "&Time=" + time , false);//false-синхронный
                     // xhr.setRequestHeader("Accept", "application/json");
                     // xhr.onload = function () {
                     // };
                     // xhr.send();
                      axios.get("api/Record" + "/newRec?Count=" + score + "&Time=" + time)
                          .then(function (response) {
                          })
                          .catch(function (error) {
                              console.log(error);
                          });

                      alert("GAME OVER");
                      z.props.history.push('/');
                  }
                  else {
                      x = canvas.width / 2;
                      y = canvas.height - 30;
                      dx = 3;
                      dy = -3;
                      ballRadius = ballRadius + 3;
                      paddleWidth = paddleWidth + 15;
                      paddleX = (canvas.width - paddleWidth) / 2;
                  }
              }
          }
     
          if (rightPressed && paddleX < canvas.width - paddleWidth) {
              paddleX += 7;
          }
          else if (leftPressed && paddleX > 0) {
              paddleX -= 7;
          }
     
          x += dx;
          y += dy;

          if (a != "false") {
              requestAnimationFrame(draw);
          }
      }
     
      draw();

    }


    public render() {
        //////////////////////////////
        ///АКСИ НЕ УМЕЮ СИНХРОННО, ДА И НЕХУЙ, ЗАПРОСЫ С НИЗУ НАДО ПЕРЕНЕСТИ!
        ////////////////////////////////
       // var xhr = new XMLHttpRequest();
       // xhr.open("get", "api/Record" + "/rec", false);//false-синхронный
       // xhr.setRequestHeader("Accept", "application/json");
       // xhr.onload = function () {
       //     str = JSON.parse(xhr.responseText);
       // };
       // xhr.send();

        return <div>
            <h1>{this.state.str}</h1>
            <h1>{this.state.str2}</h1>
                <canvas style={{ marginTop: "20%", backgroundColor: "gray" }} id="myCanvas" width="550" height="400"></canvas>   
        </div>;      
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.counter, // Selects which state properties are merged into the component's props
    CounterStore.actionCreators                 // Selects which action creators are merged into the component's props
)(GameOne) as typeof GameOne;