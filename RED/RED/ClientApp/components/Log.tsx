﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';
import * as WeatherForecasts from '../store/WeatherForecasts';
import axios from 'axios';


type CounterProps =
    CounterStore.CounterState
    & typeof CounterStore.actionCreators
    & RouteComponentProps<{}>;


interface RegState {
    log: string;
    pas: string;
    rlog: string;
    rpas: string;
    rpas2: string;
    will: string;
    will2: string;
}

 class Log extends React.Component<CounterProps, RegState > {
    constructor() {
        super();
        this.state = { rlog: "", rpas: "", rpas2: "", log: "", pas: "" ,will:"false",will2:""};
        }


    handleChange = (event: any) => {
        this.setState({ log: event.target.value });
    }

    handleChange2 = (event: any) => {
        this.setState({ pas: event.target.value });
    }

    handleChange3 = (event: any) => {
        this.setState({ rlog: event.target.value });
    }

    handleChange4 = (event: any) => {
        this.setState({rpas: event.target.value });
    }

    handleChange5 = (event: any) => {
        this.setState({ rpas2: event.target.value });
    }



    loadData = () => {
        ////////////////////////////////////////////////
        //НЕ КАНОН!!!!!!(ММММММММММММММММ!)->AXIO!  
        ///////////////////////////////////////////
   // var xhr = new XMLHttpRequest();
   // xhr.open("get", "api/my" + "/log?firstName=" + this.state.log + "&lastName=" + this.state.pas, true);//false-синхронный
   // xhr.setRequestHeader("Accept", "application/json");
   // var z = this;
   // this.setState({ will2: "false" });
   // xhr.onload = function () {
   //     var data2 = JSON.parse(xhr.responseText);
   //     localStorage.setItem('reg', data2);
   //     z.props.log(data2);
   //     z.setState({ will2: "true" });
   //   //  alert(z.props.logStatus);
   //    // z.setState({ regCount: data });
   // }.bind(this);
   // xhr.send();


        var z = this;
        axios.get("api/my" + "/log?firstName=" + this.state.log + "&lastName=" + this.state.pas)
          .then(function (response) {
              //console.log(response);
              //alert(response.data);
              localStorage.setItem('reg', response.data);
              z.props.log(response.data)
              z.setState({ will2: "true" });
          })
          .catch(function (error) {
              console.log(error);
          });
    }

    loadData2 = () => {
       // var xhr = new XMLHttpRequest();
       // xhr.open("get", "api/my" + "/reg?firstName=" + this.state.rlog + "&lastName=" + this.state.rpas + "&lastName2=" + this.state.rpas2, true);//false-синхронный
       // xhr.setRequestHeader("Accept", "application/json");
       // var z = this;
       // xhr.onload = function () {
       //     var data = JSON.parse(xhr.responseText);
       //     z.props.reg(data) 
       // }.bind(this);
       // xhr.send();
        var z = this;
        axios.get("api/my" + "/reg?firstName=" + this.state.rlog + "&lastName=" + this.state.rpas + "&lastName2=" + this.state.rpas2)
            .then(function (response) {
                z.props.reg(response.data) 
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    loadOut = () => {
        // var xhr = new XMLHttpRequest();
        // xhr.open("get", "api/my" + "/out", true);//false-синхронный
        // xhr.setRequestHeader("Accept", "application/json");
        // var z = this;
        // xhr.onload = function () {
        // }.bind(this);
        // xhr.send();
        var z = this;
        axios.get("api/my" + "/out")
            .then(function (response) {
            })
            .catch(function (error) {
                console.log(error);
            });
    
         localStorage.setItem('reg', "false");
         var data = "false";
         this.props.log(data) 
    }


    componentDidMount() {//это из жизненого цикла, еще до того как отобразилосб(к дому обращаться нельзя!!!! но круто менять статус)
     var xhr = new XMLHttpRequest();
     xhr.open("get", "api/my" + "/zap", false);//false-синхронный
     xhr.setRequestHeader("Accept", "application/json");
     var z = this;
     xhr.onload = function () {
        var data = JSON.parse(xhr.responseText);
        z.props.log(data) 
        localStorage.setItem('reg', data);
     }.bind(this);
     xhr.send();

      this.setState({ will: "true" });
    }

    public render() {
        return <div>

            {this.state.will == "true" &&
                <div>
                    <h1>Hello!</h1>
                        {localStorage.getItem('reg') != "true" &&
                         <div>
                            {this.props.logStatus == "false" &&
                                 <div>NOT OK!</div>
                            }
                            {this.state.will2 == "false" &&
                                <div>Loading.....</div>
                            }
                            <input placeholder="Login" type="text" onChange={this.handleChange} />

                            <input placeholder="Password" type="Password" onChange={this.handleChange2} />

                            <p><button onClick={this.loadData}>LOG IN</button></p>
                            {this.props.regStatus == "true" &&
                                <div>OK!</div>
                            }
                            {this.props.regStatus == "false" &&
                                <div>NOT OK!</div>
                            }

                            <input placeholder="Login" type="text" onChange={this.handleChange3} />

                            <input placeholder="Password" type="Password" onChange={this.handleChange4} />

                            <input placeholder="RepPassword" type="Password" onChange={this.handleChange5} />

                            <p><button onClick={this.loadData2}>REG</button></p>
                        </div>
                        }

                    {localStorage.getItem('reg') == "true" &&
                        <div>
                            <p><button onClick={this.loadOut}>OUT</button></p>
                        </div>
                    }

                </div>
            }

            {this.state.will == "false" &&
                <div>
                <h1>Loading.....</h1>
                </div>
            }

        </div>;
    }
}

export default connect(
    (state: ApplicationState) => state.counter,
    CounterStore.actionCreators                       
 )(Log) as typeof Log;


