﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';
import * as WeatherForecasts from '../store/WeatherForecasts';
import axios from 'axios';


type CounterProps =
    CounterStore.CounterState
    & typeof CounterStore.actionCreators
    & RouteComponentProps<{}>;

interface RegState {
    pageCount: number;
    page: number;
    imageCount: number;
    a: Array<string>;
}

class Music extends React.Component<CounterProps, RegState> {
    constructor() {
        super();
        this.state = { pageCount: 0, page: 1, imageCount: 0,a:[]};
    }
    componentDidMount() {
        this.newPage(1);//для прогрузка начального шок-контента
        var z = this;
        axios.get("api/Image" + "/count")
            .then(function (response) {
                z.setState({ pageCount: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    newPage(e: number) {
        this.setState({ page: e});
        var z = this;
        axios.get("api/Image" + "/content?page="+e)
            .then(function (response) {
                z.setState({ imageCount: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });

      axios.get("api/Image" + "/content2?page=" + e)
          .then(function (response) {
              z.setState({ a: response.data });     
          })
          .catch(function (error) {
              console.log(error);
          });
    }


    public render() {
        var lis = [];
        for (var i = 0; i < this.state.pageCount; i++) {
            lis.push(<td className='page' id={i.toString()} onClick={this.newPage.bind(this, (i+1))}>{i + 1}</td >);
        }

        var lis2 = [];
        for (var i = 0; i < this.state.imageCount; i++) {
            lis2.push(<div className={"img" + i.toString()} ><img src={this.state.a[i]} className={"img"} id={"d" + i.toString()} /></div>);
        }

        return <div style={{ width: '100%', height: '96vh' }} className='nice' >
            {lis2}
            <div className='pagen'>
                    <tr>
                        {lis}
                    </tr>
                </div>
        </div>;
    }
}


export default connect(
    (state: ApplicationState) => state.counter,
    CounterStore.actionCreators
)(Music) as typeof Music;


