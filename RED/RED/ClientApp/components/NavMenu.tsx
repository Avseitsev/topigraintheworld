﻿import * as React from 'react';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as CounterStore from '../store/Counter';
import * as WeatherForecasts from '../store/WeatherForecasts';

type CounterState1 = CounterStore.CounterState;
type CounterState2 = typeof CounterStore.actionCreators;
type CounterState3 = RouteComponentProps<{}>;
type CounterProps =
    CounterState1
    & CounterState2
    & CounterState3;


///вот тут чекать на отражение в меню, а там рпидумать радирект я даже хз и запрещать переход туда


class NavMenu extends React.Component<any, {}> {
    public render() {
        return <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                <div className='navbar-header'>
                    <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span className='sr-only'>Toggle navigation</span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>
                </div>
                <div className='clearfix'></div>
                <div className='navbar-collapse collapse'>                  
                    <ul className='nav navbar-nav'>
                        {localStorage.getItem('reg') != "true" &&
                            <li>
                                <NavLink exact to={'/'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-home'></span> LOG/REG
                            </NavLink>
                            </li>
                        }
                        {localStorage.getItem('reg') == "true" &&
                            <li>
                                <NavLink exact to={'/'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-home'></span> LOG OUT
                            </NavLink>
                            </li>
                        }
                        {localStorage.getItem('reg') == "true" &&
                            <li>
                                <NavLink exact to={'/GameOne'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-th-list'></span> PUSH-GAME
                            </NavLink>
                            </li>
                        }
                        {localStorage.getItem('reg') == "true" &&
                            <li>
                            <NavLink exact to={'/Music'} activeClassName='active'>
                                <span className='glyphicon glyphicon-th-list'></span> MUSIC
                            </NavLink>
                            </li>
                        }
                        {localStorage.getItem('reg') == "true" &&
                            <li>
                                <NavLink exact to={'/Chart'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-th-list'></span> CHAT
                            </NavLink>
                            </li>
                        }
                    </ul>
                </div>
            </div>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect<CounterState1, CounterState2, CounterState3>(
    (state: ApplicationState) => state.counter, // Selects which state properties are merged into the component's props
    CounterStore.actionCreators                 // Selects which action creators are merged into the component's props
)(NavMenu) as typeof NavMenu;