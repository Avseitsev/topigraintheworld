import * as React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Layout } from './components/Layout';
import Home from './components/Home';
import Log from './components/Log';
import Counter from './components/Counter';
import GameOne from './components/GameOne';
import Music from './components/Music';
import Chart from './components/Chart';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import * as CounterStore from './store/Counter';
import * as WeatherForecasts from './store/WeatherForecasts';
import { render } from 'react-dom';


function requireAuth() {
    console.log("called"); 
}




export const routes = <Layout>
    <Route exact path='/' component={Log} />
    <Route exact path='/GameOne' component={GameOne}/>
    <Route exact path='/Music' component={Music} />
    <Route exact path='/Chart' component={Chart} />
</Layout>;

