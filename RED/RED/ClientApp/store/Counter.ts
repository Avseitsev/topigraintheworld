import { Action, Reducer } from 'redux';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface CounterState {
    count: number;
    logStatus: string;
    regStatus: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface IncrementCountAction { type: 'INCREMENT_COUNT' }
interface Increment2CountAction { type: 'INCREMENT_COUNT2' }
interface DecrementCountAction { type: 'DECREMENT_COUNT' }
interface LogAction { type: 'LOG', a: any }
interface RegAction { type: 'REG', z: any }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = IncrementCountAction | DecrementCountAction | Increment2CountAction | LogAction | RegAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    increment: () => <IncrementCountAction>{ type: 'INCREMENT_COUNT' },
    increment2: () => <Increment2CountAction>{ type: 'INCREMENT_COUNT2' },
    decrement: () => <DecrementCountAction>{ type: 'DECREMENT_COUNT' },
    log: (a: any) => <LogAction>{ type: 'LOG', a },
    reg: (z: any) => <RegAction>{ type: 'REG', z }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<CounterState> = (state: CounterState, action: KnownAction) => {
    switch (action.type) {
        case 'INCREMENT_COUNT':
            return { count: state.count + 1, logStatus: state.logStatus, regStatus: state.regStatus };
        case 'INCREMENT_COUNT2':
            return { count: state.count + 2, logStatus: state.logStatus, regStatus: state.regStatus };
        case 'DECREMENT_COUNT':
            return { count: state.count - 1, logStatus: state.logStatus, regStatus: state.regStatus };
        case 'LOG':
            return { count: state.count, logStatus: action.a, regStatus: state.regStatus};
        case 'REG':
            return { count: state.count, logStatus: state.logStatus , regStatus: action.z };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || { count: 0, logStatus: "null", regStatus: "null" };
};
