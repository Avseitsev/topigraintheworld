﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace RED.Controllers
{
    public class HomeController : Controller
    {
            //крч, во 2 контройлере надо еще парамтером имя передавать для связи и проверять что бы не сбивалось или я хз по реге проверять!!! но что бы переза8гузкой не сбивало. и кнопку назад!
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Index2()
        {
            return View("Index2", User.Identity.Name);
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
