﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace RED.Controllers
{
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        [HttpGet("count")]
        public string Rec()
        {
            string[] fileEntries = Directory.GetFiles("wwwroot/Image/");
            double s = (double) fileEntries.Count() / 6;

            return Math.Ceiling(s).ToString();
        }

        [HttpGet("content")]
        public string Rec(int page)
        {
            page = (page-1) * 6;
            List<string> fileEntries = Directory.GetFiles("wwwroot/Image/").ToList(); ;
            fileEntries = fileEntries.Skip(page).Take(6).ToList();
            return fileEntries.Count().ToString();
        }


        [HttpGet("content2")]
        public List<string> Rec2(int page)
        {
            page = (page - 1) * 6;
            List<string> fileEntries = Directory.GetFiles("wwwroot/Image/").ToList(); ;
            fileEntries = fileEntries.Skip(page).Take(6).ToList();
            for(int i=0;i< fileEntries.Count();i++)
            {
                fileEntries[i] = fileEntries[i].Remove(0,7);
            }
            return fileEntries;
        }

    }
}
