﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RED.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace RED.Controllers
{
    [Route("api/[controller]")]
    public class MyController : Controller
    {

        private UserContext db;
        public MyController(UserContext context)
        {
            db = context;
        }

        [HttpGet]
        public string Get()
        {
            return "true";
        }
        [HttpGet("zap")]
        public string Zap()
        {
            if (User.Identity.IsAuthenticated)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }


        [HttpGet("log")]
        public string Get(string firstName, string lastName)
        {
            if (firstName!=null && lastName!=null)
            {
                User user = db.Users.FirstOrDefault(u => u.Email == firstName && u.Password == lastName);
                if (user != null)
                {
                    Logout();
                   Authenticate(firstName); 
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "false";
            }
        }

        private void Authenticate(string userName)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        [HttpGet("reg")]
        public string Get(string firstName, string lastName, string lastName2)
        {
            try
            {
                if (firstName != null && lastName != null && lastName2 != null && lastName2 == lastName)
                {
                    User user = db.Users.FirstOrDefault(u => u.Email == firstName);
                    if (user == null)
                    {
                        db.Users.Add(new User { Email = firstName, Password = lastName });
                        db.SaveChanges();
                        User userNew = db.Users.FirstOrDefault(u => u.Email == firstName);
                        db.UserRecords.Add(new UserRecord { User=userNew,Time=0,Count=0});
                        db.SaveChanges();

                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }

        [HttpGet("out")]
        public void Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        [HttpGet("name")]
        public string Name()
        {
            try
            {
                return User.Identity.Name;
            }
            catch
            {
                return "";
            }
        }
    }
}
