﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RED.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;


namespace RED.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class RecordController : Controller
    {

        private UserContext db;
        public RecordController(UserContext context)
        {
            db = context;
        }


        [HttpGet("rec")]     
        public string Rec()
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            UserRecord userRecord = db.UserRecords.FirstOrDefault(u => u.User==user);
            return "Your record of "+ userRecord.Count + " beams in " + userRecord.Time + "seconds";
        }


        [HttpGet("recW")]
        public string RecW()
        {
            db.Users.ToList();
            User nea = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (db.WorldRecords.Count()==0)
            {
                User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
                db.WorldRecords.Add(new WorldRecord { User = user, Time = 0, Count = 0,Date= DateTime.Now });
                db.SaveChanges();
            }

            return "World record of " + db.WorldRecords.ToList()[0].Count + " beams in " + db.WorldRecords.ToList()[0].Time + " seconds(Player:" + db.WorldRecords.ToList()[0].User.Email + " Date:"+ db.WorldRecords.ToList()[0].Date + ")" ;
        }


        [HttpGet("newRec")]
        public void NewRec(int Count, int Time)
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            UserRecord userRecord = db.UserRecords.FirstOrDefault(u => u.User == user);

            if(userRecord.Count<Count){
                userRecord.Count = Count;
                userRecord.Time = Time;
            }
            else if (userRecord.Count == Count)
            {
                if (userRecord.Time > Time)
                {
                    userRecord.Time = Time;
                }
            }
            db.SaveChanges();



            WorldRecord worldRecord = db.WorldRecords.ToList()[0];
            if (worldRecord.Count < Count)
            {
               worldRecord.Count = Count;
               worldRecord.Time = Time;
               worldRecord.User = user;
               worldRecord.Date = DateTime.Now;
            }
            else if (worldRecord.Count == Count)
            {
                if (worldRecord.Time > Time)
                {
                    worldRecord.Time = Time;
                    worldRecord.User = user;
                    worldRecord.Date = DateTime.Now;
                }
            }
            db.SaveChanges();
        }
    }
}
