﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RED.Models
{
    public class UserRecord
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int Time { get; set; }
        public int Count { get; set; }
    }
}
