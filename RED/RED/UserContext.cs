﻿using Microsoft.EntityFrameworkCore;
using RED.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RED
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserRecord> UserRecords { get; set; }
        public DbSet<WorldRecord> WorldRecords { get; set; }
       

        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
        }
    }
}
